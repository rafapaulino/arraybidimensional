﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayBiDimensional
{
    class Program
    {
        static void Main(string[] args)
        {
            double[,] alunoNota = new double[3, 3];

            alunoNota[0, 0] = 8;
            alunoNota[0, 1] = 5;
            alunoNota[0, 2] = 9;

            alunoNota[1, 0] = 6;
            alunoNota[1, 1] = 7;
            alunoNota[1, 2] = 10;

            alunoNota[2, 0] = 7;
            alunoNota[2, 1] = 8;
            alunoNota[2, 2] = 9;

            for (int aluno = 0; aluno < 3; aluno++)
            {
                for (int nota = 0; nota < 3; nota++)
                {
                    Console.WriteLine("Array Matriz Aluno: [" + aluno + "] - Nota: [" + nota + "] = " + alunoNota[aluno, nota]);
                }
            }

            Console.ReadKey();
        }
    }
}
